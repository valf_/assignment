var Node = function ( name, parent, id )
{
    this.id = id || Math.round( Date.now() * Math.random() );
    this.children = [];
    this.name = name || 'node_' + this.id.toString().substr(0, 5);

    if ( parent )
    {
        this.parent = parent;
        this.parent.children.push( this );
    }
};

Node.root = null;

Node.prototype.html = function ( wrap_in_ul_tag )
{
    var html = '';
    
    html += '<li data-id="' + this.id + '" data-name="' + this.name + '">'
    html += '<span class="name">' + this.name + '</span>';
    html += '<span class="add">+</span>';
    
    if( !this.is_root() )
    {
        html += '<span class="remove">-</span>';
    }
    
    if ( this.children.length )
    {
        html += '<ul>';
        for( var i = 0; i < this.children.length; i++ )
        {
            html += this.children[i].html();
        }
        html += '</ul>';
    }
    
    html += '</li>';
    
    if ( wrap_in_ul_tag )
    {
        html = '<ul>' + html + '</ul>';
    }

    return html;
};

Node.prototype.each = function ( callback )
{
    var stack = [ this ];
    var current;
    
    while( current = stack.pop() )
    {
        if ( callback(current) === false )
        {
            break;
        }
        
        stack = stack.concat( current.children );
    }
};

Node.prototype.find = function ( id )
{
    var node_to_be_found;
    
    this.each( function ( node )
    {
        if ( node.id == id )
        {
            node_to_be_found = node;
            return false;
        }
    });
    
    return node_to_be_found;
};

Node.prototype.find_by_name = function ( name )
{
    var node_to_be_found;
    
    this.each( function ( node )
    {
        if ( node.name === name )
        {
            node_to_be_found = node;
            return false;
        }
    });
    
    return node_to_be_found;
};

Node.prototype.is_root = function ()
{
    return !this.parent;
};

Node.prototype.remove = function ()
{
    if( this.is_root() )
    {
        throw new Error('root node cannot be removed');
    }
    else
    {
        for ( var i = 0; this.parent.children.length; i++ )
        {
            if ( this.parent.children[i].id == this.id )
            {
                this.parent.children.splice( i, 1 );
                break;
            }
        }
    }
};

Node.prototype.toString = function ( wrap_in_curlies )
{
    var string = '"' + this.name + '":';
    
    if ( this.children.length )
    {
        string += '{';
        for( var i = 0; i < this.children.length; i++ )
        {
            string += this.children[i].toString( false ) + ',';
        }
        string = string.slice(0, -1); // remove last comma
        string += '}';
    }
    else
    {
        string += 'null';
    }
    
    if ( wrap_in_curlies !== false )
    {
        string = '{' + string + '}';
    }
    
    return string;
};
