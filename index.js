$( function ()
{
    var $controller = $('.tree-controller');
    var tree = localStorage.getItem('tree');

    if( !tree )
    {
        tree = { 'root': { 'parent_1': null, 'parent_2': null, 'parent_3': null } };
        localStorage.setItem( 'tree', JSON.stringify( tree ));
    }
    
    var restore_tree = function ()
    {
        var root_name;
        
        tree = JSON.parse( localStorage.getItem('tree') );    
        
        for( var name in tree )
        {
            if ( !root_name )
            {
                root_name = name;
                break;
            }
        }
        
        Node.root = new Node( root_name );
    
        var walk_tree_recursive = function ( node, parent )
        {
            for (var key in node)
            {
                var child = new Node( key, parent );
                
                if ( typeof node[key] === 'object' )
                {
                    walk_tree_recursive(node[key], child);
                }
            }
        };
    
        walk_tree_recursive( tree[ root_name ], Node.root );
    
        $controller.find('ul').remove();
        $controller.append( Node.root.html( true ) );
    }
    
    restore_tree();
    
    $controller.on( 'click', '.add', function ()
    {
        var $self = $(this);
        var $parent = $self.parent();
        var node_id = $parent.attr('data-id');
        var node = Node.root.find( node_id );
        var child = new Node( null, node );
        
        if ( $parent.children('ul').length )
        {
            $parent.children('ul').append( child.html() );
        }
        else
        {
            $parent.append( child.html( true ) );
        }
        
        $controller.trigger( 'node_add', [ child ] );
    });
    
    $controller.on( 'click', '.remove', function ()
    {
        var $parent = $(this).parent();
        var node_id = $parent.attr('data-id');
        var node = Node.root.find( node_id );
        
        $parent.remove();
        node.remove();
    });

    $controller.on( 'click', '.save', function ()
    {
        localStorage.setItem( 'tree', Node.root.toString() );
    });

    $controller.on( 'click', '.restore', function ()
    {
        tree = JSON.parse( localStorage.getItem('tree') );
        restore_tree();
    });
    
});
