describe("Node", function()
{    
    var root = new Node('root');
    var child_id = Math.round( Date.now() * Math.random() );
    var child;
    
    it("root node doesn't find a non-existand child", function ()
    {
        expect( root.find( child_id ) ).toBe( undefined );
    });
    
    it("root node finds child by id", function()
    {
        child = new Node(null, root, child_id);
        expect( root.find( child.id ) ).not.toBe( undefined );
    });
    
    it("child node is removed", function()
    {
        child.remove();
        expect( root.children.length ).toBe( 0 );
    });
    
});

describe("DOM", function()
{
    var $controller = $('.tree-controller');
    var $restore = $('.restore');
    var $save = $('.save');
    var child;
    
    $controller.on('node_add', function ( event, node )
    {
        child = node;
    });
    
    it("restores the tree to default state", function ()
    {
        $('.tree-controller').find('ul').remove();
        localStorage.setItem( 'tree', JSON.stringify( { 'root': { 'parent_1': null, 'parent_2': null } } ));
        $restore.trigger('click');
        expect( $('.tree-controller').find('li').length ).toBe( 3 );
    });
    
    it("inserts new child in DOM", function ()
    {
        $controller.find('.add').first().trigger('click');
        expect( $('.tree-controller').find('li').length ).toBe( 4 );
    });
    
    it("newly created child is saved and restored", function ()
    {
        $save.trigger('click');
        $('.tree-controller').find('ul').remove();
        $restore.trigger('click');
        $child = $('.tree-controller').find('li[data-name="' + child.name + '"]');
        expect( $child.length ).toBe( 1 );
    });
    
    it("newly created child is found in root node", function ()
    {
        expect( Node.root.find_by_name( child.name ) ).not.toBe( undefined );
    });
    
});